import { NgModule } from '@angular/core';

import { TerminIfOnDomDirective } from '@termin/directives/termin-if-on-dom/termin-if-on-dom.directive';
import { TerminInnerScrollDirective } from '@termin/directives/termin-inner-scroll/termin-inner-scroll.directive';
import { TerminPerfectScrollbarDirective } from '@termin/directives/termin-perfect-scrollbar/termin-perfect-scrollbar.directive';
import { TerminMatSidenavHelperDirective, TerminMatSidenavTogglerDirective } from '@termin/directives/termin-mat-sidenav/termin-mat-sidenav.directive';

@NgModule({
    declarations: [
        TerminIfOnDomDirective,
        TerminInnerScrollDirective,
        TerminMatSidenavHelperDirective,
        TerminMatSidenavTogglerDirective,
        TerminPerfectScrollbarDirective
    ],
    imports     : [],
    exports     : [
        TerminIfOnDomDirective,
        TerminInnerScrollDirective,
        TerminMatSidenavHelperDirective,
        TerminMatSidenavTogglerDirective,
        TerminPerfectScrollbarDirective
    ]
})
export class TerminDirectivesModule
{
}
