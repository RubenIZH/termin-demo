import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MatDividerModule, MatListModule } from '@angular/material';

import { TerminDemoContentComponent } from './demo-content/demo-content.component';
import { TerminDemoSidebarComponent } from './demo-sidebar/demo-sidebar.component';

@NgModule({
    declarations: [
        TerminDemoContentComponent,
        TerminDemoSidebarComponent
    ],
    imports     : [
        RouterModule,

        MatDividerModule,
        MatListModule
    ],
    exports     : [
        TerminDemoContentComponent,
        TerminDemoSidebarComponent
    ]
})
export class TerminDemoModule
{
}
