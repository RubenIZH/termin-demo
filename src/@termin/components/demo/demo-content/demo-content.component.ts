import { Component } from '@angular/core';

@Component({
    selector   : 'termin-demo-content',
    templateUrl: './demo-content.component.html',
    styleUrls  : ['./demo-content.component.scss']
})
export class TerminDemoContentComponent
{
    /**
     * Constructor
     */
    constructor()
    {
    }
}
