import { NgModule } from '@angular/core';

import { TerminHighlightComponent } from '@termin/components/highlight/highlight.component';

@NgModule({
    declarations: [
        TerminHighlightComponent
    ],
    exports: [
        TerminHighlightComponent
    ],
})
export class TerminHighlightModule
{
}
