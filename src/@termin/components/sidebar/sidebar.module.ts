import { NgModule } from '@angular/core';

import { TerminSidebarComponent } from './sidebar.component';

@NgModule({
    declarations: [
        TerminSidebarComponent
    ],
    exports     : [
        TerminSidebarComponent
    ]
})
export class TerminSidebarModule
{
}
