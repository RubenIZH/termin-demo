import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
    selector   : 'termin-confirm-dialog',
    templateUrl: './confirm-dialog.component.html',
    styleUrls  : ['./confirm-dialog.component.scss']
})
export class TerminConfirmDialogComponent
{
    public confirmMessage: string;

    /**
     * Constructor
     *
     * @param {MatDialogRef<TerminConfirmDialogComponent>} dialogRef
     */
    constructor(
        public dialogRef: MatDialogRef<TerminConfirmDialogComponent>
    )
    {
    }

}
