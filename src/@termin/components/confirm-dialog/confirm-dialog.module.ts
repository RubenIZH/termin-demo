import { NgModule } from '@angular/core';
import { MatButtonModule, MatDialogModule } from '@angular/material';

import { TerminConfirmDialogComponent } from '@termin/components/confirm-dialog/confirm-dialog.component';

@NgModule({
    declarations: [
        TerminConfirmDialogComponent
    ],
    imports: [
        MatDialogModule,
        MatButtonModule
    ],
    entryComponents: [
        TerminConfirmDialogComponent
    ],
})
export class TerminConfirmDialogModule
{
}
