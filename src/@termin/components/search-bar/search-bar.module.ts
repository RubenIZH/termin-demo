import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { MatButtonModule, MatIconModule } from '@angular/material';

import { TerminSearchBarComponent } from './search-bar.component';

@NgModule({
    declarations: [
        TerminSearchBarComponent
    ],
    imports     : [
        CommonModule,
        RouterModule,

        MatButtonModule,
        MatIconModule
    ],
    exports     : [
        TerminSearchBarComponent
    ]
})
export class TerminSearchBarModule
{
}
