import { NgModule } from '@angular/core';

import { TerminWidgetComponent } from './widget.component';
import { TerminWidgetToggleDirective } from './widget-toggle.directive';

@NgModule({
    declarations: [
        TerminWidgetComponent,
        TerminWidgetToggleDirective
    ],
    exports     : [
        TerminWidgetComponent,
        TerminWidgetToggleDirective
    ],
})
export class TerminWidgetModule
{
}
