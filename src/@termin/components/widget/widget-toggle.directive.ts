import { Directive, ElementRef } from '@angular/core';

@Directive({
    selector: '[terminWidgetToggle]'
})
export class TerminWidgetToggleDirective
{
    /**
     * Constructor
     *
     * @param {ElementRef} elementRef
     */
    constructor(
        public elementRef: ElementRef
    )
    {
    }
}
