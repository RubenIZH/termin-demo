import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MatIconModule, MatRippleModule } from '@angular/material';

import { TranslateModule } from '@ngx-translate/core';

import { TerminNavigationComponent } from './navigation.component';
import { TerminNavVerticalItemComponent } from './vertical/item/item.component';
import { TerminNavVerticalCollapsableComponent } from './vertical/collapsable/collapsable.component';
import { TerminNavVerticalGroupComponent } from './vertical/group/group.component';
import { TerminNavHorizontalItemComponent } from './horizontal/item/item.component';
import { TerminNavHorizontalCollapsableComponent } from './horizontal/collapsable/collapsable.component';

@NgModule({
    imports     : [
        CommonModule,
        RouterModule,

        MatIconModule,
        MatRippleModule,

        TranslateModule.forChild()
    ],
    exports     : [
        TerminNavigationComponent
    ],
    declarations: [
        TerminNavigationComponent,
        TerminNavVerticalGroupComponent,
        TerminNavVerticalItemComponent,
        TerminNavVerticalCollapsableComponent,
        TerminNavHorizontalItemComponent,
        TerminNavHorizontalCollapsableComponent
    ]
})
export class TerminNavigationModule
{
}
