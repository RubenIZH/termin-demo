import { NgModule } from '@angular/core';

import { TerminCountdownComponent } from '@termin/components/countdown/countdown.component';

@NgModule({
    declarations: [
        TerminCountdownComponent
    ],
    exports: [
        TerminCountdownComponent
    ],
})
export class TerminCountdownModule
{
}
