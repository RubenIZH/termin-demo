import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { MatButtonModule, MatIconModule, MatProgressBarModule } from '@angular/material';

import { TerminProgressBarComponent } from './progress-bar.component';

@NgModule({
    declarations: [
        TerminProgressBarComponent
    ],
    imports     : [
        CommonModule,
        RouterModule,

        MatButtonModule,
        MatIconModule,
        MatProgressBarModule
    ],
    exports     : [
        TerminProgressBarComponent
    ]
})
export class TerminProgressBarModule
{
}
