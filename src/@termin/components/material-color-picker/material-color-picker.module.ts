import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule, MatIconModule, MatMenuModule, MatTooltipModule } from '@angular/material';

import { TerminPipesModule } from '@termin/pipes/pipes.module';

import { TerminMaterialColorPickerComponent } from '@termin/components/material-color-picker/material-color-picker.component';

@NgModule({
    declarations: [
        TerminMaterialColorPickerComponent
    ],
    imports: [
        CommonModule,

        FlexLayoutModule,

        MatButtonModule,
        MatIconModule,
        MatMenuModule,
        MatTooltipModule,

        TerminPipesModule
    ],
    exports: [
        TerminMaterialColorPickerComponent
    ],
})
export class TerminMaterialColorPickerModule
{
}
