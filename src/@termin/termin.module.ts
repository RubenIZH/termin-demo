import { ModuleWithProviders, NgModule, Optional, SkipSelf } from '@angular/core';

import { TERMIN_CONFIG } from '@termin/services/config.service';

@NgModule()
export class TerminModule
{
    constructor(@Optional() @SkipSelf() parentModule: TerminModule)
    {
        if ( parentModule )
        {
            throw new Error('TerminModule is already loaded. Import it in the AppModule only!');
        }
    }

    static forRoot(config): ModuleWithProviders
    {
        return {
            ngModule : TerminModule,
            providers: [
                {
                    provide : TERMIN_CONFIG,
                    useValue: config
                }
            ]
        };
    }
}
