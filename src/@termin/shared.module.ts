import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { FlexLayoutModule } from '@angular/flex-layout';

import { TerminDirectivesModule } from '@termin/directives/directives';
import { TerminPipesModule } from '@termin/pipes/pipes.module';

@NgModule({
    imports  : [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,

        FlexLayoutModule,

        TerminDirectivesModule,
        TerminPipesModule
    ],
    exports  : [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,

        FlexLayoutModule,

        TerminDirectivesModule,
        TerminPipesModule
    ]
})
export class TerminSharedModule
{
}
