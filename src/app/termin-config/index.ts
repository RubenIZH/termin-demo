import { TerminConfig } from '@termin/types';

/**
 * Default Termin Configuration
 *
 * You can edit these options to change the default options. All these options also can be
 * changed per component basis. See `app/main/pages/authentication/login/login.component.ts`
 * constructor method to learn more about changing these options per component basis.
 */

export const terminConfig: TerminConfig = {
    // Color themes can be defined in src/app/app.theme.scsss
    colorTheme      : 'theme-blue-gray-dark',
    customScrollbars: true,
    layout          : {
        style    : 'vertical-layout-3',
        width    : 'fullwidth',
        navbar   : {
            primaryBackground  : 'termin-navy-700',
            secondaryBackground: 'termin-navy-900',
            folded             : false,
            hidden             : false,
            position           : 'left',
            variant            : 'vertical-style-1'
        },
        toolbar  : {
            customBackgroundColor: false,
            background           : 'termin-white-500',
            hidden               : false,
            position             : 'above-static'
        },
        footer   : {
            customBackgroundColor: true,
            background           : 'termin-navy-900',
            hidden               : false,
            position             : 'below-fixed'
        },
        sidepanel: {
            hidden  : false,
            position: 'right'
        }
    }
};
