import { NgModule } from '@angular/core';

import { VerticalLayout3Module } from 'app/layout/vertical/layout-3/layout-3.module';


@NgModule({
    imports: [
        VerticalLayout3Module
    ],
    exports: [
        VerticalLayout3Module
    ]
})
export class LayoutModule
{
}
