import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TerminSharedModule } from '@termin/shared.module';

import { ContentComponent } from 'app/layout/components/content/content.component';

@NgModule({
    declarations: [
        ContentComponent
    ],
    imports     : [
        RouterModule,
        TerminSharedModule
    ],
    exports     : [
        ContentComponent
    ]
})
export class ContentModule
{
}
