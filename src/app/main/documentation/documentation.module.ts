import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MatIconModule } from '@angular/material';

import { TerminSharedModule } from '@termin/shared.module';

const routes: Routes = [

    {
        path        : 'working-with-termin',
        loadChildren: './working-with-termin/working-with-termin.module#WorkingWithTerminModule'
    }
];

@NgModule({
    declarations: [

    ],
    imports     : [
        RouterModule.forChild(routes),

        MatIconModule,

        TerminSharedModule
    ]
})
export class DocumentationModule
{
}
