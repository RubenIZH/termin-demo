import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatButtonModule, MatIconModule } from '@angular/material';

import { TerminSharedModule } from '@termin/shared.module';
import { TerminHighlightModule } from '@termin/components';

import { DocsWorkingWithTerminServerComponent } from 'app/main/documentation/working-with-termin/server/server.component';
import { DocsWorkingWithTerminProductionComponent } from 'app/main/documentation/working-with-termin/production/production.component';

const routes = [
    {
        path     : 'server',
        component: DocsWorkingWithTerminServerComponent
    },
    {
        path     : 'production',
        component: DocsWorkingWithTerminProductionComponent
    }
];

@NgModule({
    declarations: [
        DocsWorkingWithTerminServerComponent,
        DocsWorkingWithTerminProductionComponent,
    ],
    imports     : [
        RouterModule.forChild(routes),

        MatButtonModule,
        MatIconModule,

        TerminSharedModule,
        TerminHighlightModule
    ]
})
export class WorkingWithTerminModule
{
}
