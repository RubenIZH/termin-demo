import { Component } from '@angular/core';

@Component({
    selector   : 'docs-working-with-termin-production',
    templateUrl: './production.component.html',
    styleUrls  : ['./production.component.scss']
})
export class DocsWorkingWithTerminProductionComponent
{
    constructor()
    {
    }
}
