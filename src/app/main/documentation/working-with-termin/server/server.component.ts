import { Component } from '@angular/core';

@Component({
    selector   : 'docs-working-with-termin-server',
    templateUrl: './server.component.html',
    styleUrls  : ['./server.component.scss']
})
export class DocsWorkingWithTerminServerComponent
{
    constructor()
    {
    }
}
