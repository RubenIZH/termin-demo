import { Component } from '@angular/core';

import { TerminSidebarService } from '@termin/components/sidebar/sidebar.service';

@Component({
    selector   : 'carded-left-sidebar-tabbed-1',
    templateUrl: './left-sidebar-tabbed-1.component.html',
    styleUrls  : ['./left-sidebar-tabbed-1.component.scss']
})
export class CardedLeftSidebarTabbed1Component
{
    /**
     * Constructor
     *
     * @param {TerminSidebarService} _terminSidebarService
     */
    constructor(
        private _terminSidebarService: TerminSidebarService
    )
    {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Toggle sidebar
     *
     * @param name
     */
    toggleSidebar(name): void
    {
        this._terminSidebarService.getSidebar(name).toggleOpen();
    }
}
