import { Component } from '@angular/core';

import { TerminSidebarService } from '@termin/components/sidebar/sidebar.service';

@Component({
    selector   : 'carded-right-sidebar-tabbed-1',
    templateUrl: './right-sidebar-tabbed-1.component.html',
    styleUrls  : ['./right-sidebar-tabbed-1.component.scss']
})
export class CardedRightSidebarTabbed1Component
{
    /**
     * Constructor
     *
     * @param {TerminSidebarService} _terminSidebarService
     */
    constructor(
        private _terminSidebarService: TerminSidebarService
    )
    {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Toggle sidebar
     *
     * @param name
     */
    toggleSidebar(name): void
    {
        this._terminSidebarService.getSidebar(name).toggleOpen();
    }
}
