import { Component } from '@angular/core';

import { TerminSidebarService } from '@termin/components/sidebar/sidebar.service';

@Component({
    selector   : 'simple-right-sidebar-1',
    templateUrl: './right-sidebar-1.component.html',
    styleUrls  : ['./right-sidebar-1.component.scss']
})
export class SimpleRightSidebar1Component
{
    /**
     * Constructor
     *
     * @param {TerminSidebarService} _terminSidebarService
     */
    constructor(
        private _terminSidebarService: TerminSidebarService
    )
    {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Toggle sidebar
     *
     * @param name
     */
    toggleSidebar(name): void
    {
        this._terminSidebarService.getSidebar(name).toggleOpen();
    }
}
