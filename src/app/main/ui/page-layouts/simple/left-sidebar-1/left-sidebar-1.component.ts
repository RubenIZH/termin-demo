import { Component } from '@angular/core';

import { TerminSidebarService } from '@termin/components/sidebar/sidebar.service';

@Component({
    selector   : 'simple-left-sidebar-1',
    templateUrl: './left-sidebar-1.component.html',
    styleUrls  : ['./left-sidebar-1.component.scss']
})
export class SimpleLeftSidebar1Component
{
    /**
     * Constructor
     *
     * @param {TerminSidebarService} _terminSidebarService
     */
    constructor(
        private _terminSidebarService: TerminSidebarService
    )
    {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Toggle sidebar
     *
     * @param name
     */
    toggleSidebar(name): void
    {
        this._terminSidebarService.getSidebar(name).toggleOpen();
    }
}
