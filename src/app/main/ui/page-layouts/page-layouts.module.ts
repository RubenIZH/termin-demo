import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MatButtonModule, MatIconModule, MatTabsModule } from '@angular/material';

import { TerminSharedModule } from '@termin/shared.module';
import { TerminDemoModule } from '@termin/components/demo/demo.module';

import { CardedFullWidth1Component } from 'app/main/ui/page-layouts/carded/full-width-1/full-width-1.component';
import { CardedFullWidthTabbed1Component } from 'app/main/ui/page-layouts/carded/full-width-tabbed-1/full-width-tabbed-1.component';
import { CardedLeftSidebar1Component } from 'app/main/ui/page-layouts/carded/left-sidebar-1/left-sidebar-1.component';
import { CardedLeftSidebarTabbed1Component } from 'app/main/ui/page-layouts/carded/left-sidebar-tabbed-1/left-sidebar-tabbed-1.component';
import { CardedRightSidebar1Component } from 'app/main/ui/page-layouts/carded/right-sidebar-1/right-sidebar-1.component';
import { CardedRightSidebarTabbed1Component } from 'app/main/ui/page-layouts/carded/right-sidebar-tabbed-1/right-sidebar-tabbed-1.component';
import { SimpleFullWidth1Component } from 'app/main/ui/page-layouts/simple/full-width-1/full-width-1.component';
import { SimpleFullWidthTabbed1Component } from 'app/main/ui/page-layouts/simple/full-width-tabbed-1/full-width-tabbed-1.component';
import { SimpleLeftSidebar1Component } from 'app/main/ui/page-layouts/simple/left-sidebar-1/left-sidebar-1.component';

import { SimpleRightSidebar1Component } from 'app/main/ui/page-layouts/simple/right-sidebar-1/right-sidebar-1.component';


import { TerminSidebarModule } from '@termin/components';

const routes: Routes = [
    // Carded
    {
        path     : 'page-layouts/carded/full-width-1',
        component: CardedFullWidth1Component
    },
    {
        path     : 'page-layouts/carded/full-width-tabbed-1',
        component: CardedFullWidthTabbed1Component
    },
    {
        path     : 'page-layouts/carded/left-sidebar-1',
        component: CardedLeftSidebar1Component
    },
    {
        path     : 'page-layouts/carded/left-sidebar-tabbed-1',
        component: CardedLeftSidebarTabbed1Component
    },
    {
        path     : 'page-layouts/carded/right-sidebar-1',
        component: CardedRightSidebar1Component
    },
    {
        path     : 'page-layouts/carded/right-sidebar-tabbed-1',
        component: CardedRightSidebarTabbed1Component
    },

    // Simple
    {
        path     : 'page-layouts/simple/full-width-1',
        component: SimpleFullWidth1Component
    },
    {
        path     : 'page-layouts/simple/full-width-tabbed-1',
        component: SimpleFullWidthTabbed1Component
    },
    {
        path     : 'page-layouts/simple/left-sidebar-1',
        component: SimpleLeftSidebar1Component
    },
    {
        path     : 'page-layouts/simple/right-sidebar-1',
        component: SimpleRightSidebar1Component
    }
];

@NgModule({
    declarations: [
        CardedFullWidth1Component,
        CardedFullWidthTabbed1Component,
        CardedLeftSidebar1Component,
        CardedLeftSidebarTabbed1Component,
        CardedRightSidebar1Component,
        CardedRightSidebarTabbed1Component,
        SimpleFullWidth1Component,
        SimpleFullWidthTabbed1Component,
        SimpleLeftSidebar1Component,
        SimpleRightSidebar1Component,
    ],
    imports     : [
        RouterModule.forChild(routes),

        MatButtonModule,
        MatIconModule,
        MatTabsModule,

        TerminSidebarModule,
        TerminSharedModule,
        TerminDemoModule
    ]
})
export class UIPageLayoutsModule
{
}
