import { NgModule } from '@angular/core';

import { UIPageLayoutsModule } from 'app/main/ui/page-layouts/page-layouts.module';

@NgModule({
    imports: [

        UIPageLayoutsModule

    ]
})
export class UIModule
{
}
