import { TerminPage } from './app.po';

describe('Termin App', () => {
    let page: TerminPage;

    beforeEach(() => {
        page = new TerminPage();
    });

    it('should display welcome message', () => {
        page.navigateTo();
        expect(page.getParagraphText()).toEqual('Welcome to Termin!');
    });
});
